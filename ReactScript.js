/**
 * Created by bunea on 19.06.2017.
 */

var start = new Date().getTime();

function makeShapeAppear() {

    document.getElementById("shape").style.display = "block";

    start = new Date().getTime();

    var resize = (Math.random() * 400);

    if(Math.random() > 0.5){

        document.getElementById("shape").style.borderRadius = "50%";

    } else{

        document.getElementById("shape").style.borderRadius = "0";

    }


    document.getElementById("shape").style.backgroundColor = getRandomColor();
    document.getElementById("shape").style.top = (Math.random() * 400 + 5) + "px";
    document.getElementById("shape").style.left = (Math.random() * 400 + 5) + "px";
    document.getElementById("shape").style.display = "block";
    document.getElementById("shape").style.width =  resize + "px";
    document.getElementById("shape").style.height = resize + "px";

}

function getRandomColor() {
    var letters = '0123456789ABCDEF';

    var color = '#';

    for (var i = 0; i < 6; i++ ) {

        color += letters[Math.floor(Math.random() * 16)];

    }

    return color;
}


function appearAfterDelay(){

    setTimeout(makeShapeAppear, ( Math.random() * 2000 ));



}

appearAfterDelay();


document.getElementById("shape").onclick = function () {

    document.getElementById("shape").style.display = "none";

    var end = new Date().getTime();

    var timeTaken = (end - start) / 1000; // take seconds

    document.getElementById("timeTaken").innerHTML = timeTaken + "s";

    appearAfterDelay();



};